from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn import preprocessing
from sklearn import metrics
import pandas as pd


class NB:

    def __init__(self):
        self.data = self.load_data()
        self.le = preprocessing.LabelEncoder()
        self.execute()

    @staticmethod
    def load_data():
        return pd.read_csv("../docs/house-votes-84.data", header=None)

    def execute(self):

        labels = self.le.fit_transform(self.data.pop(0))
        data = self.data.apply(self.le.fit_transform).to_numpy()

        # Split dataset into training set and test set 80/20
        X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.2)

        model = GaussianNB()

        model.fit(X_train, y_train)

        y_pred = model.predict(X_test)

        print("Accuracy:", metrics.accuracy_score(y_test, y_pred))


NB()
